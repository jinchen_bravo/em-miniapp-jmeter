const Mustache = require('mustache');
const fs = require('fs');

const TV_JMETER_TEMPLATE = 'jmeter-table-validation.mustache';
const TV_JMX = 'log/table-validation.jmx';
const template = fs.readFileSync(TV_JMETER_TEMPLATE, 'utf8');
const threadsNumber = process.env.EM_MINIAPP_TVLOADTHREADS
  ? parseInt(process.env.EM_MINIAPP_TVLOADTHREADS, 10)
  : 1;
const dockersOnVMHost = process.env.EM_MINIAPP_HOST
  ? process.env.EM_MINIAPP_HOST
  : 'sdl33364.labs.teradata.com';
const kubeClusterHost = process.env.EM_MINIAPP_HOST
  ? process.env.EM_MINIAPP_HOST
  : 'sdl36238.labs.teradata.com';
const tvLoopCountAll = process.env.EM_MINIAPP_TV_LOOP_COUNT_ALL
  ? parseInt(process.env.EM_MINIAPP_TV_LOOP_COUNT_ALL, 10)
  : 1;
const tvStatusWhileLoopCount = process.env.EM_MINIAPP_TV_STATUS_WHILE_LOOP_COUNT
  ? { count: parseInt(process.env.EM_MINIAPP_TV_STATUS_WHILE_LOOP_COUNT, 10) }
  : false;
const tvTableType = process.env.EM_MINIAPP_TV_TABLE_TYPE
  ? process.env.EM_MINIAPP_TV_TABLE_TYPE
  : '5K';
let tvMetric = process.env.EM_MINIAPP_TV_METRIC_TYPE
  ? process.env.EM_MINIAPP_TV_METRIC_TYPE
  : 'ROWCOUNT';
if (
  tvMetric.toUpperCase() !== 'ROWCOUNT' &&
  tvMetric.toUpperCase() !== 'ROWCOUNTPCT' &&
  tvMetric.toUpperCase() !== 'CHECKSUM'
) {
  throw new Error(`Invalid table_metric_name : ${tvMetric}`);
} else {
  switch (tvMetric) {
    case 'ROWCOUNT':
      tvMetric = { name: 'ROWCOUNT' };
      break;
    case 'ROWCOUNTPCT':
      tvMetric = { name: 'ROWCOUNTPCT' };
      break;
    case 'CHECKSUM':
      tvMetric = { name: 'CHECKSUM', sql: 'sum(item)' };
      break;
    default:
      tvMetric = { name: 'ROWCOUNT' };
  }
}
const threadsArray = [];
let count = 1;
let breakFlag = false;
const TOTAL_DB = 10;
const TABLE_PER_DB = 10;
for (let i = 1; i <= TOTAL_DB; i += 1) {
  for (let j = 1; j <= TABLE_PER_DB; j += 1) {
    const tmp = i.toString().padStart(3, 0);
    const tmp1 = j.toString().padStart(3, 0);
    const t = {};
    t.db = `tvperf${tvTableType}${tmp}`;
    t.table = `tvtbl${tmp1}`;
    threadsArray.push(t);
    count += 1;
    if (threadsNumber === count - 1) {
      breakFlag = true;
      break;
    }
  }
  if (breakFlag) break;
}
let config;
if (process.env.EM_MINIAPP_DEPLOY === 'dockers-on-vm') {
  config = {
    protocol: 'https',
    host: dockersOnVMHost,
    table_validation_service_port: 7220,
    user_service_port: 8001,
    user_service_path_prefix: undefined,
    table_validation_service_path_prefix: undefined,
  };
} else {
  config = {
    protocol: 'https',
    host: kubeClusterHost,
    table_validation_service_port: 443,
    user_service_port: 443,
    user_service_path_prefix: '/api/user',
    table_validation_service_path_prefix: '/api/tablevalidation',
  };
}
const view = {
  em_mini_app: config,
  user_service: { username: 'root', password: 'Teradata1!' },
  setup_and_teardown: threadsArray,
  with_counter_while_status: tvStatusWhileLoopCount,
  loop_count_for_all: tvLoopCountAll,
  table_metric: tvMetric,
};
const data = Mustache.render(template, view);
fs.writeFileSync(TV_JMX, data, 'utf8');
