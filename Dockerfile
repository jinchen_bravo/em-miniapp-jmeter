FROM sdartifact.td.teradata.com:7002/ae/emminiapp/jmeterbase:latest

COPY ./ /em-miniapp-jmeter
WORKDIR /em-miniapp-jmeter
RUN make deps
ENTRYPOINT exec make test
