.DEFAULT_GOAL := test
include ./env_file.mk
gen : deps
	node jmeterGen.js
test : gen
	jmeter -n -t log/table-validation.jmx -l log/table-validation.txt -j log/jmeter.log
jenkins : gen
	jmeter -Jjmeter.save.saveservice.output_format=xml -n -t log/table-validation.jmx -l log/table-validation.jtl -j log/jmeter.log
deps :
	npm i;mkdir -p log
build : login
	docker-compose build --no-cache
run : rmdir
	docker-compose up --exit-code-from $(DOCKER_INSTANCE) --force-recreate $(DOCKER_INSTANCE)
prune :
	docker system prune -f
clean :
	docker-compose rm -f
rmdir :
	rm -rf report;rm -rf log
login :
	docker login $(DOCKER_URL) -u $(DOCKER_USER) -p $(DOCKER_PASS)
logout :
	docker logout $(DOCKER_URL)
push : login
	docker push $(DOCKER_URL)/$(DOCKER_IMAGE)
pull : login
	docker pull $(DOCKER_URL)/$(DOCKER_IMAGE)
